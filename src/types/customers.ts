type Results = {
  createdAt: string;
  updatedAt: string;
  uuid: string;
  name: string;
  shippingAddress: string;
  billingAddress: string;
  invoiceSubInstructions: string;
}

export type Customers = {
  result: Results[];
  count: number;
  limit: number;
  offset: number;
};

import request from "../utils/request";

export function getCustomers(accessToken: string) {
  return new Promise((resolve, reject) => {
    request({
      method: "get",
      url: "/api/customers",
      token: accessToken,
    })
      .then((response) => {
        if (response.status === 200 || response.status === 201)
          resolve(response);
        reject(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

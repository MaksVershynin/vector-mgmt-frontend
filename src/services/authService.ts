import {
  ResetPasswordType,
  SetNewPasswordType,
  SignInType,
  SignUpType,
} from "../types/auth";

import request from "../utils/request";

export function signIn(credentials: SignInType) {
  return new Promise((resolve, reject) => {
    request({
      method: "post",
      url: "/api/auth/sign-in",
      body: JSON.stringify(credentials),
    })
      .then((response: any) => {
        if (response.status === 200 || response.status === 201) {
          resolve(response);
        }
        reject(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export function signUp(credentials: SignUpType) {
  return new Promise((resolve, reject) => {
    request({
      method: "post",
      url: "/api/auth/sign-up",
      body: JSON.stringify(credentials),
    })
      .then((res: any) => res.json())
      .then((data) => {
        if (data.code === 10001) reject(data);
        resolve(data);
      })
      .catch((err) => reject(err));
  });
}

export function resetPassword(credentials: ResetPasswordType) {
  return new Promise((resolve, reject) => {
    request({
      method: "post",
      url: "/api/auth/restore-password",
      body: JSON.stringify(credentials),
    })
      .then((res: any) => res.json())
      .then((data) => {
        if (data.code === 10002)
          reject({ message: "This email are not registered" });
        resolve(data);
      })
      .catch((err) => reject(err));
  });
}

export function setNewPassword(credentials: SetNewPasswordType) {
  return new Promise((resolve, reject) => {
    request({
      method: "post",
      url: "/api/auth/set-password",
      body: JSON.stringify({ password: credentials.password }),
      token: credentials.resetToken,
    })
      .then((response) => {
        if (response.status === 200 || response.status === 201)
          resolve(response);
        reject(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

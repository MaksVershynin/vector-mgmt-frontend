import React from "react";
import styled, { createGlobalStyle } from "styled-components/macro";
import { GlobalStyleProps } from "../types/styles";
import { CssBaseline } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

import themeIndexTypography from "../theme/typography";
import themeIndexPalette from "../theme/variants";
import { THEMES } from "../constants/index";

const GlobalStyle = createGlobalStyle<GlobalStyleProps>`
  html,
  body,
  #root {
    height: 100%;
  }

  body {
    background: ${(props) => props.theme.palette.background.default};
  }
`;

const Root = styled.div`
  max-width: 520px;
  margin: 0 auto;
  justify-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;
  min-height: 100%;
`;

const theme = createMuiTheme({
  palette: themeIndexPalette.filter((theme) => theme.name === THEMES.AUTH)[0]
    .palette,
  typography: themeIndexTypography,
});

const Auth: React.FC = ({ children }) => {
  return (
    <Root>
      <CssBaseline />
      <GlobalStyle />
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </Root>
  );
};

export default Auth;

import { combineReducers } from "redux";

import themeReducer from "./themeReducer";
import authReducer from "./authReducer";
import customersReducer from "./customersReducer";

export const rootReducer = combineReducers({
  themeReducer,
  authReducer,
  customersReducer,
});

type RootReducerType = typeof rootReducer;
export type AppStateType = ReturnType<RootReducerType>;

import * as types from "../../constants";
import { Customers } from "../../types/customers";

const initState = {
  customers: {
    count: null,
    limit: null,
    offset: null,
    results: null,
  },
  isCustomersRequest: false,
};
export default function reducer(
  state = initState,
  actions: { type: string; payload: Customers }
): any {
  switch (actions.type) {
    case types.GET_CUSTOMERS_REQUEST:
      return {
        ...state,
        isCustomersRequest: true,
      };

    case types.GET_CUSTOMERS_SUCCESS:
      return {
        ...state,
        customers: actions.payload,
        isCustomersRequest: false,
      };

    case types.GET_CUSTOMERS_FAILURE:
      return {
        ...state,
        customers: actions.payload,
        isCustomersRequest: false,
      };

    default:
      return state;
  }
}

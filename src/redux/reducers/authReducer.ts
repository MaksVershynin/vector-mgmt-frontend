import * as types from "../../constants";

export type UserType = {
  id?: string;
  email?: string;
  name?: string;
  uuid?: string;
  role?: string;
  location?: string;
};

export type PayloadType = {};

export type AuthType = {
  user?: UserType | undefined;
  resetPasswordRequest?: string;
  isSignRequest?: boolean;
};

export default function reducer(
  state = {},
  actions: UserType & { type: string } & { payload?: any | PayloadType }
): AuthType {
  switch (actions.type) {
    // SIGN IN
    case types.AUTH_SIGN_IN_REQUEST:
      return {
        ...state,
        isSignRequest: true,
      };

    case types.AUTH_SIGN_IN_SUCCESS:
      return {
        ...state,
        user: {
          id: actions.id,
          email: actions.email,
          name: actions.name,
          location: actions.location,
          role: actions.role,
        },
        isSignRequest: false,
      };

    case types.AUTH_SIGN_IN_FAILURE:
      return {
        ...state,
        isSignRequest: false,
      };

    // RESET PASSWORD
    case types.AUTH_RESET_PASSWORD_REQUEST:
      return {
        ...state,
        resetPasswordRequest: types.AUTH_RESET_PASSWORD_REQUEST,
      };

    case types.AUTH_RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        resetPasswordRequest: types.AUTH_RESET_PASSWORD_SUCCESS,
      };

    case types.AUTH_RESET_PASSWORD_FAILURE:
      return {
        ...state,
        resetPasswordRequest: types.AUTH_RESET_PASSWORD_FAILURE,
      };

    // SIGN OUT
    case types.AUTH_SIGN_OUT:
      return {
        ...state,
        user: actions.payload,
      };

    default:
      return state;
  }
}

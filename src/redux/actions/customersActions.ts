import * as types from "../../constants";
import { AppDispatchType } from "../store";
import { Customers } from "../../types/customers";
import { getCustomers as getCustomers_request } from "../../services/customersService";
import { RemoveShoppingCartRounded } from "@material-ui/icons";

export function getCustomers(accessToken: any) {
  return async (dispatch: AppDispatchType) => {
    dispatch({ type: types.GET_CUSTOMERS_REQUEST });

    return getCustomers_request(accessToken)
      .then((response: any) => response.json())
      .then((response: any) => {
        dispatch({
          type: types.GET_CUSTOMERS_SUCCESS,
          payload: {
            results: response.results,
            count: response.count,
            limit: response.limit,
            offset: response.offset,
          },
        });
      })
      .catch((error) => {
        dispatch({ type: types.GET_CUSTOMERS_FAILURE });
        throw error;
      });
  };
}

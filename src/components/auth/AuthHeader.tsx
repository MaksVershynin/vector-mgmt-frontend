import React from "react";
import styled from "styled-components/macro";

const Header = styled.header`
  background-color: #4d84c0;
  width: ${(props) => props.theme.spacing(96)}px;
  height: 68px;
  font-size: 22px;
  font-family: Montserrat;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  ${(props) => props.theme.breakpoints.up("md")} {
    width: ${(props) => props.theme.spacing(114)}px;
    font-size: 32px;
  } ;
`;

const AuthHeader = () => {
  return <Header>Vector DynamixE MGMT</Header>;
};

export default AuthHeader;

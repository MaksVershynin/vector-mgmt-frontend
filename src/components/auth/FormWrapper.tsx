import styled from "styled-components/macro";
import { Paper } from "@material-ui/core";

const FormWrapper = styled(Paper)`
  padding: ${(props) => props.theme.spacing(6)}px;
  width: ${(props) => props.theme.spacing(96)}px;
  height: auto;
  ${(props) => props.theme.breakpoints.up("md")} {
    padding: ${(props) => props.theme.spacing(10)}px;
    width: ${(props) => props.theme.spacing(114)}px;
  }
`;

export default FormWrapper;

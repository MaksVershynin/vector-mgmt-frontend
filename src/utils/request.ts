import { hostname } from "../constants/index";

interface Parameters {
  method: string;
  url: string;
  headers?: any;
  body?: string;
  token?: string;
}

const refreshToken = localStorage.getItem("refreshToken");

const refreshTokenFunctionality = async (parameters: Parameters) => {
  const headers = {
    ...parameters.headers,
    "Content-Type": "application/json",
    Accept: "application/json",
  };
  const authHeaders = {
    ...headers,
    Authorization: `Bearer ${parameters.token}`,
  };

  const updateAccessToken = async () => {
    return await fetch(hostname + "/api/auth/refresh", {
      method: "post",
      headers: {
        ...headers,
        Authorization: `Bearer ${refreshToken}`,
      },
    })
      .then((res) => {
        if (res.status === 401) return res;
        return res;
      })
      .then((res) => res.json())
      .then(async (data) => {
        const newAccessToken = data.accessToken;
        localStorage.setItem("accessToken", newAccessToken);
        return await fetch(hostname + parameters.url, {
          method: parameters.method,
          headers: {
            ...headers,
            Authorization: `Bearer ${newAccessToken}`,
          },
          body: parameters.body,
        })
          .then((res) => res)
          .catch((err) => err);
      })
      .catch((err) => err);
  };

  return await fetch(hostname + parameters.url, {
    method: parameters.method,
    headers: parameters.token ? { ...authHeaders } : { ...headers },
    body: parameters.body,
  })
    .then(async (res) => {
      if (res.status === 401) return await updateAccessToken();
      return res;
    })
    .catch((err) => err);
};

export default refreshTokenFunctionality;

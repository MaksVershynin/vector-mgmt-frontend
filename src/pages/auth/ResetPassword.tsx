import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import styled from "styled-components/macro";
import { Helmet } from "react-helmet";
import * as Yup from "yup";
import { Formik } from "formik";
import { resetPassword, setNewPassword } from "../../redux/actions/authActions";
import { AppStateType } from "../../redux/reducers/index";
import * as types from "../../constants";

import AuthHeader from "../../components/auth/AuthHeader";
import FormWraper from "../../components/auth/FormWrapper";

import {
  Button,
  TextField as MuiTextField,
  Typography,
} from "@material-ui/core";
import { spacing } from "@material-ui/system";
import { Alert as MuiAlert } from "@material-ui/lab";

const Alert = styled(MuiAlert)(spacing);

const TextField = styled(MuiTextField)<{ my?: number }>(spacing);

const ButtonGroup = styled.div`
  display: flex;
  justify-content: center;
  & button {
    width: fit-content;
    margin: 0 7px;
  }
  margin-top: 51px;
`;

function ResetPassword() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isQueryInURL, setQueryInURL] = React.useState(false);
  const resetToken = new URLSearchParams(history.location.search).get(
    "passwordResetToken"
  );

  const resetPasswordRequest = useSelector(
    (state: AppStateType) => state.authReducer.resetPasswordRequest
  );

  //Handle redirect to SignIn screen, once requests to restore/set_new pass succeeded
  React.useEffect(() => {
    resetPasswordRequest === types.AUTH_RESET_PASSWORD_SUCCESS &&
      dispatch({ type: types.AUTH_RESET_PASSWORD_REQUEST }) &&
      history.push("/auth/sign-in");
  }, [resetPasswordRequest]);

  //Handle changing input fields according to the "passwordResetToken"
  //"passwordResetToken" --> coming from backend as auth token. URL query parameter taken from email link
  React.useEffect(() => {
    resetToken !== null && setQueryInURL(true);
  }, [resetToken]);

  return (
    <React.Fragment>
      <AuthHeader />
      <FormWraper>
        {/* 1st screen - RESTORE Password*/}
        <Helmet title="Reset Password" />
        <Typography component="h1" variant="h4" align="center" gutterBottom>
          Forgot Password
        </Typography>
        <Typography component="h2" variant="body1" align="center">
          {!isQueryInURL
            ? "Enter your email below to receive a reset password link"
            : "Enter a new password below to reset it"}
        </Typography>
        {!isQueryInURL ? (
          <Formik
            initialValues={{
              email: "",
              submit: false,
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string()
                .email("Must be a valid email")
                .max(255)
                .required("Email is required"),
            })}
            onSubmit={async (
              values,
              { setErrors, setStatus, setSubmitting }
            ) => {
              try {
                await dispatch(resetPassword({ email: values.email }));
              } catch (error) {
                const message = error.message || "Something went wrong";
                setStatus({ success: false });
                setErrors({ submit: message });
                setSubmitting(false);
              }
            }}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              touched,
              values,
            }) => (
              <form noValidate onSubmit={handleSubmit}>
                {errors.submit && (
                  <Alert mt={2} mb={1} severity="warning">
                    {errors.submit}
                  </Alert>
                )}
                <TextField
                  type="email"
                  name="email"
                  label="Email Address"
                  value={values.email}
                  error={Boolean(touched.email && errors.email)}
                  fullWidth
                  helperText={touched.email && errors.email}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  my={3}
                />
                <ButtonGroup>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={isSubmitting}
                  >
                    Reset password
                  </Button>
                  <Button
                    fullWidth
                    variant="contained"
                    color="secondary"
                    onClick={() => history.push("/auth/sign-in")}
                  >
                    Cancel
                  </Button>
                </ButtonGroup>
              </form>
            )}
          </Formik>
        ) : (
          // 2nd screen - SET NEW Password
          <Formik
            initialValues={{
              password: "",
              confirmPassword: "",
              submit: false,
            }}
            validationSchema={Yup.object().shape({
              password: Yup.string()
                .min(12, "Must be at least 12 characters")
                .max(255)
                .required("Required"),
              confirmPassword: Yup.string()
                .when("password", {
                  is: (val: any) => (val && val.length > 0 ? true : false),
                  then: Yup.string().oneOf(
                    [Yup.ref("password")],
                    "Both password need to be the same"
                  ),
                })
                .required("Required"),
            })}
            onSubmit={async (
              values,
              { setErrors, setStatus, setSubmitting }
            ) => {
              try {
                await dispatch(
                  setNewPassword({
                    password: values.confirmPassword,
                    resetToken: resetToken || "",
                  })
                );
              } catch (error) {
                const message = error.message || "Something went wrong";
                setStatus({ success: false });
                setErrors({ submit: message });
                setSubmitting(false);
              }
            }}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              touched,
              values,
            }) => (
              <form noValidate onSubmit={handleSubmit}>
                {errors.submit && (
                  <Alert mt={2} mb={1} severity="warning">
                    {errors.submit}
                  </Alert>
                )}
                <TextField
                  type="password"
                  name="password"
                  label="New Password"
                  value={values.password}
                  error={Boolean(touched.password && errors.password)}
                  fullWidth
                  helperText={touched.password && errors.password}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  my={3}
                />
                <TextField
                  type="password"
                  name="confirmPassword"
                  label="Confirm New Password"
                  value={values.confirmPassword}
                  error={Boolean(
                    touched.confirmPassword && errors.confirmPassword
                  )}
                  fullWidth
                  helperText={touched.confirmPassword && errors.confirmPassword}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  my={3}
                />
                <ButtonGroup>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={isSubmitting}
                  >
                    Set New Password
                  </Button>
                  <Button
                    fullWidth
                    variant="contained"
                    color="secondary"
                    onClick={() => history.push("/auth/sign-in")}
                  >
                    Cancel
                  </Button>
                </ButtonGroup>
              </form>
            )}
          </Formik>
        )}
      </FormWraper>
    </React.Fragment>
  );
}

export default ResetPassword;
